#include "main.hpp"

int main(const int argc, const char* argv[])
{
	if (ss::args::Check(argc, argv) == false) return false;	
	return ss::Main::Run(ss::args::m_args[ss::args::mc_parm_in].c_str(), ss::args::m_args[ss::args::mc_parm_out].c_str());
}

bool ss::Main::Run(CoCStr_t in_path, CoCStr_t out_path)
{
	std::string path_json_ok;
	if (ss::utls::SetQuotes(in_path, path_json_ok))
	{	
		std::string json_stream;
		if (ss::utls::GetFile(path_json_ok.c_str(), json_stream) == false) return false;
		JSONNode n = libjson::parse(json_stream.c_str());				
		std::ofstream os;
		if (ss::ParseJSON(n, ss::ROOT, os, out_path) == false) return false;
		std::cout << "Ficheros generados con exito en : " << out_path << std::endl;
	} else return false;

	return true;
}
