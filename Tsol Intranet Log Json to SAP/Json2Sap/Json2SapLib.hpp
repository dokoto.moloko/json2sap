#ifndef JSON2SAPLIB_HPP
#define JSON2SAPLIB_HPP

#include "libjson.h"
#include "GlobTypes.hpp"
#include "utils.hpp"
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

namespace ss
{

namespace utls
{
	bool SetQuotes(const char* path, std::string& path_out);
	void RemoveSpaces(std::string& line);
}

	enum	{ROOT, NODE, ARRAY, VALUE};
	bool ParseJSON(const JSONNode & n, const int parent, std::ofstream& os, CoCStr_t path_out);
}


#endif //JSON2SAPLIB_HPP