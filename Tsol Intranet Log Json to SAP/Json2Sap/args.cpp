#include "args.hpp"

namespace ss
{

args::Args_t args::m_args;
CoUInt_t args::m_oblig = 2;

Str_t ss::args::m_use = "\
Use: %s [-modo ] -in [\"Ruta Fichero Entrada\"] -out [\"Ruta Directorio Salida\"]\n\
	 Ej: %s -in \"C:\\DIR1\\Fichero.txt\"  -out \"C:\\DIR2\\\"\n\
";

CoCStr_t args::mc_parm_in = "-in";
CoCStr_t args::mc_parm_out = "-out";
CoCStr_t args::mc_parm_prog = "prog";

	bool args::Check(CoInt_t argc, CoCStr_t argv[])
	{		
		if (load(argc, argv) == false) return false;
		UInt_t oblig = 0, opc = 0;
		for ( Args_t::iterator it = ss::args::m_args.begin(); it != ss::args::m_args.end(); it++)
		{
			if (it->first.compare(mc_parm_in) == 0)
			{
				if (valida1(it->second) == false) return false;
				oblig++;         
			}
			else if (it->first.compare(mc_parm_out) == 0)        
			{
				if (valida2(it->second) == false) return false;
				oblig++;
			}
			else if (it->first.compare(mc_parm_prog) == 0)
				continue;
			else
			{
				std::cout <<  "Parametros no reconocido : " << it->first << " Use --help." << std::endl;
				return false;
			}
		}

		if (oblig != ss::args::m_oblig)
		{
			std::cout <<  "Faltan parametros obligatorios. Use --help." << std::endl;
			return false;
		}
		return true;
	}

	bool args::valida1(CoStr_t& valor)
	{
		std::ifstream inFile(valor.c_str());
		if (inFile.fail())
		{
			Buffer_t ss;
			ss << "El fichero no parece existir en : " << valor << std::endl;
			std::cout << ss.str() << std::endl;
			return false;
		}
		inFile.close();

		return true;
	}

	bool args::valida2(CoStr_t& valor)
	{
		if ( valor.substr(valor.size()-1, 1).compare(utls::GetDirSepToStr()) == 0 )
		{
			std::cout <<  "No use separador de directorio al final del parametro -out" << std::endl;
			return false;
		}
		if ( valor.substr(valor.size()-1, 1).compare("\"") == 0 )
		{
			std::cout <<  "Se ha colado una comilla en la ruta del parametro -out, no use : '\\ULTIMO_DIR\\\"' , porque indica que la comilla de cierre es parte de la ruta, use : '\\ULTIMO_DIR\"'." << std::endl;
			return false;
		}

		return true;
	}


	Str_t args::GetProgName(CoCStr_t arg)
	{
		Str_t prog_name(arg);
		Str_t like_unix = prog_name.substr(0, 1);
		Str_t like_win  = prog_name.substr(1, 2);

		if ( like_unix.compare("/") == 0 )
			prog_name = prog_name.substr(prog_name.find_last_of("/")+1, prog_name.size());
		else if ( like_win.compare(":\\") == 0 ) 
			prog_name = prog_name.substr(prog_name.find_last_of("\\")+1, prog_name.size());

		return prog_name;
	}

	bool args::load(CoInt_t argc, CoCStr_t argv[])
	{
		
		size_t pos = 0;
		while ((pos = m_use.find("%s")) != Str_t::npos) m_use.replace(pos, 2, GetProgName(argv[0]));

		if (argc == 1)
		{
			std::cerr << m_use << std::endl;
			return false;
		}

		for (UInt_t i = 0; i < SC(size_t, argc-1);)
		{
			if (i == 0)
			{
				m_args[mc_parm_prog] = Str_t(argv[i]);
				i++;
			}
			else
			{
				m_args[Str_t(argv[i])] = Str_t(argv[i+1]);
				i += 2;
			}
		} 

		return true;
	}
}
