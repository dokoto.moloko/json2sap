#include "Json2SapLib.hpp"

namespace ss
{
namespace utls
{
	void RemoveSpaces(std::string& line)
	{
		for(size_t i = 0; i < line.size(); i++)
		{
			if (line[i] == ' ')
			{
				line.erase(i, 1);
				i--;
			}
		}
	}

	bool SetQuotes(const char* path, std::string& path_out)
	{
		std::ifstream is(path);
		if (is.is_open())
		{			
			is.seekg(0, std::ios::beg);
			std::stringstream ss;
			ss << is.rdbuf();
			is.close();
			std::string line = ss.str();		
			RemoveSpaces(line);
			size_t pos_ini = 0;
			char car_ini = '\0';
			bool entre_palabras = false;
			for(size_t i = 0; i < line.size(); i++)
			{
				if (car_ini == ':' && line[i] == '{')
				{
					pos_ini = i;
					car_ini = line[i];
					entre_palabras = true;
					continue;
				}

				if (car_ini == ',' && line[i] == '{')
				{
					pos_ini = i;
					car_ini = line[i];
					entre_palabras = true;
					continue;
				}

				if( (line[i] == '{' || line[i] == ':' || line[i] == ',') && entre_palabras == false)
				{
					pos_ini = i;
					car_ini = line[i];
					entre_palabras = true;
					continue;
				}

				if( (line[i] == '}' || line[i] == ':' || line[i] == ',') && entre_palabras == true)
				{
					if ( 
						(car_ini == '{' && line[i] == ':')
						||
						(car_ini == ':' && line[i] == ',')
						||
						(car_ini == ':' && line[i] == '}')
						||
						(car_ini == ',' && line[i] == ':')
						)
					{

						line.insert(pos_ini+1, "\"");
						i++;
						line.insert(i, "\"");
					}

					entre_palabras = false;
					pos_ini = 0;
					car_ini = '\0';								
				}
			}

			path_out.append(path);
			path_out.append("_with_quotes.json");
			std::ofstream os(path_out.c_str());
			if(os.is_open())
			{
				os << line;
				os.close();
				std::cout << "Generado con exito el fichero Json con Comillas en : " << path_out << std::endl;
			} 
			else
			{
				std::cerr << "No ha sido posible generar el fichero Json con comillas." << std::endl;
				return false;
			}

		}
		else
		{
			std::cerr << "No ha sido posible abrir el fichero : " << path << std::endl;
			return false;
		}

		return true;
	}
}

	bool ParseJSON(const JSONNode & n, const int parent, std::ofstream& os, CoCStr_t path_out)
	{
		JSONNode::const_iterator i = n.begin();
		while (i != n.end())
		{
			if (i->type() == JSON_ARRAY)
			{
				if (os.is_open()) os.close();
				Str_t path( path_out + utls::GetDirSepToStr() + i->name() );
				os.open( path.c_str() );
				if (!os.is_open())
				{
					std::cout << "No se ha podido generar el fichero en : " << path << std::endl;
					return false;
				}
				if (ParseJSON(*i, ss::ARRAY, os, path_out) == false) return false;
			}
			else if (i->type() == JSON_NODE) // Nombre de Fichero
			{
				if (parent == ss::ROOT)
				{
					if (os.is_open()) os.close();
					Str_t path(path_out + utls::GetDirSepToStr() + i->name() );		
					os.open( path.c_str() );			
					if (!os.is_open()) 
					{
						std::cout << "No se ha podido generar el fichero en : " << path << std::endl;
						return false;
					}
				}				
				if (ParseJSON(*i, ss::ARRAY, os, path_out) == false) return false;							
				UInt_t pos = os.tellp();
				os.seekp(pos-1);
				os << std::endl;			
			}
			else if (i->type() == JSON_NULL)
			{
				char c = i->type();
			}
			else if (i->type() == JSON_STRING) // Par nombre : valor
			{				
				os << i->as_string() << '\t';			
			}
			else if (i->type() == JSON_NUMBER)
			{
				char c = i->type();
			}
			else if (i->type() == JSON_BOOL)
			{
				char c = i->type();
			}
			++i;
		}

		return true;
	}
}
