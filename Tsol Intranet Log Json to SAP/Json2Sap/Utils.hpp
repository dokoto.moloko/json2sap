#ifndef UTILS_HPP
#define UTILS_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#ifdef _MSC_VER
	#define _CRT_SECURE_NO_WARNINGS
    #include <direct.h>
    #define GetCurrentDir _getcwd
#else
    #include <unistd.h>
    #define GetCurrentDir getcwd
 #endif

#include <cstdio>
#include <cstring>

#include <string>
#include <vector>

#include "GlobTypes.hpp"

namespace ss 
{
	namespace utls 
	{
		/*--------------------------------------------------------------------------------------------------------------
		* TYPES
		*--------------------------------------------------------------------------------------------------------------*/			
		typedef std::vector<Str_t>								VSplit_t;

		/*--------------------------------------------------------------------------------------------------------------
		* FUNCTIONS
		*--------------------------------------------------------------------------------------------------------------*/
		bool					Split							(CoChar_t sep, CoCStr_t raw, VSplit_t& split_values);
		Str_t					CurrentDir						(void);
		size_t					NumOfDirLevels					(void);
		Char_t					GetDirSep						(void);
		Str_t					GetDirSepToStr					(void);

		/* DIRLEVEL(CInt_t& Level, Str_t& Dir)
		* Si tenemos el Directorio C:\uno\dos\tres\cuatro y queremos obtener la ruta hasta C:\uno\dos
		* le pasamos (N�NIVELES - 2), es decir, dos niveles arriba
		* Ej:
		* Str_t& Dir;
		* DirLevel(NumOfDirLevels() - 2, Dir);
		*/
		Str_t					DirLevel						(CoUInt_t Level);		
		Str_t					FixWinPath						(CoStr_t& path);	
		Str_t					ExtractEXTENSION				(CoStr_t& path);
		bool					GetFile							(CoStr_t& path, Str_t& stream);
	}
}

#endif //UTILS_HPP

