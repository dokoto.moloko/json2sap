#ifndef GLOBTYPES_HPP
#define GLOBTYPES_HPP
/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#include <cstring>
#include <string>
#include <sstream>
#include <fstream>
#include <locale>

/*--------------------------------------------------------------------------------------------------------------
 * MACROS
 *--------------------------------------------------------------------------------------------------------------*/
#define SC(Tipo, Valor) static_cast<Tipo>(Valor)

typedef char											Char_t;
typedef const char										CoChar_t;
typedef std::string										Str_t;
typedef const std::string								CoStr_t;
typedef char*											CStr_t;
typedef const char*										CoCStr_t;
typedef	int												Int_t;
typedef const int										CoInt_t;
typedef size_t											UInt_t;
typedef const size_t									CoUInt_t;
typedef std::stringstream								Buffer_t;

#endif // GLOBTYPES_HPP

