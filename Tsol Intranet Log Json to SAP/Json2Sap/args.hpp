#ifndef ARGS_HPP
#define ARGS_HPP

/*--------------------------------------------------------------------------------------------------------------
 * INCLUDES
 *-------------------------------------------------------------------------------------------------------------*/
#include <string>
#include <map>
#include <fstream>
#include <iostream>

#include "GlobTypes.hpp"
#include "Utils.hpp"

namespace ss
{
	/*--------------------------------------------------------------------------------------------------------------
    * CLASS: ARGS
    *--------------------------------------------------------------------------------------------------------------*/  
	class args
    {
    /*--------------------------------------------------------------------------------------------------------------
    * PRIVATE TYPES
    *--------------------------------------------------------------------------------------------------------------*/  
	private:
		typedef std::map<Str_t, Str_t>							Args_t;		

	/*--------------------------------------------------------------------------------------------------------------
    * PRIVATE ATTRIBUTES
    *--------------------------------------------------------------------------------------------------------------*/  
	private:
		static Str_t											m_use;
		static CoUInt_t											m_oblig;

	/*--------------------------------------------------------------------------------------------------------------
    * PRIVATE METHODS
    *--------------------------------------------------------------------------------------------------------------*/  
	private:
								args							(void) {}
								~args							(void) {}
		static bool				load							(CoInt_t argc, CoCStr_t argv[]);
		static Str_t			GetProgName						(CoCStr_t arg);
		static bool				valida1							(CoStr_t& valor);
		static bool				valida2							(CoStr_t& valor);

	/*--------------------------------------------------------------------------------------------------------------
    * PUBLIC ATTRIBUTES
    *--------------------------------------------------------------------------------------------------------------*/  
	public:
		static Args_t											m_args;
		static CoCStr_t											mc_parm_in;
		static CoCStr_t											mc_parm_out;
		static CoCStr_t											mc_parm_prog;

	/*--------------------------------------------------------------------------------------------------------------
    * PUBLIC METHODS
    *--------------------------------------------------------------------------------------------------------------*/  
	public:
		static bool				Check							(CoInt_t argc, CoCStr_t argv[]);
	
	};

}

#endif //ARGS_HPP
